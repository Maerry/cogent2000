%function [exp] = experiment(sub);
sub = 4;
%% Configure Cogent
config_display(0, 5, [0.6 0.6 0.6], [1 1 1], 'Helvetica', 30, 6, 0);	% Configure display (this is set to grey background and white text)
config_keyboard;                                                        % Configure keyboard
% config_sound;                                                         % Configure sound
% config_serial;                                                        % Configure serial port (e.g., if connecting to external device, like MRI scanner)

%% Set all experimental variables (everything defined in 'exp' structure)

rng('shuffle');                                                         % Seed random number generator with current time (or use "rand('state', sum(100*clock))")
exp.ntrials       = 4;                                                  % Number of trials
exp.fname         = sprintf('exp_sub%02d.mat',sub);                     % Results filename
exp.wdir          = cd;                                                 % Working directory
exp.stimdir       = 'stimuli';                                          % Stimulus directory

exp.stim.fname    = {'pic1.bmp'; 'pic2.bmp'; 'pic3.bmp'; 'pic4.bmp'};	% Stimulus filenames
exp.stim.categ    = [1 1 2 2];                                          % Category (man-made/living) for each stimulus
exp.stim.ord      = randperm(exp.ntrials);                              % Randomise stimulus order

exp.loc           = [-200 200];                                         % x-position for object presentation (i.e., left or right of fixation)
exp.wordcond      = [1 1 2 2];                                          % 'left'/'right' word condition - note, all conditions could be randomised, or counterbalanced, but here I have hardcoded
exp.loccond       = [1 2 1 2];                                          % left/right object presentation
exp.cond          = [1:4];                                              % overall condition number (i.e., combination of word and location condition)

exp.words         = {'left'; 'right'};                                  % 'left' and 'right' strings for word presentation
exp.times.fix     = 500;                                                % Duration of fixation cross
exp.times.word    = 1000;                                               % Duration of word presentation
exp.times.blank1  = 500;                                                % Duration of 1st blank screen
exp.times.obj     = 2000;                                               % Duration of stimulus presentation
exp.times.blank2  = 1000;                                               % Duration of 2nd blank screen

%% Run the experiment
start_cogent;                                                                   % Start Cogent

clearpict(1);                                                                   % Clear buffer 1
preparestring('Ready to start?', 1);                                            % Prepare string in buffer 1
drawpict (1);                                                                   % Present buffer 1
waitkeydown(inf,71);                                                            % Wait inf for keypress (space)
clearpict(1);                                                                   % Clear buffer 1

for trial = 1:exp.ntrials                                                       % Start trial loop
    
    % Clear all buffers
    clearpict(1);                                                               % Clear buffer 1 (for fixation cross)
    clearpict(2);                                                               % Clear buffer 2 (for word presentation)
    clearpict(3);                                                               % Clear buffer 3 (for object presentation)
    clearpict(4);                                                               % Clear buffer 4 (for blank screen)
    
    % Prepare all stimuli in buffers
    preparestring('+',1)                                                        % Prepare buffer 1 (fixation cross)
    preparestring(exp.words{exp.wordcond(trial)},2)                             % Prepare buffer 2 (word presentation)
    loadpict(...                                                                % Prepare image presentation...
        fullfile(exp.wdir,exp.stimdir,exp.stim.fname{exp.stim.ord(trial)}),...  % Filename for image...
        3,...                                                                   % In buffer 3...
        exp.loc(exp.loccond(trial)),0);                                         % In the correct x-axis position (y=0)
    preparestring('is the object man-made?',3,0,-200);                          % Prepare string in buffer 3 (task question)
    preparestring('YES        NO',3,0,-250);                                    % Prepare string in buffer 3 (task responses)
    
    % Run trial
    drawpict(1);                                                                % Present fixation cross
    wait(exp.times.fix);                                                        % Wait
    drawpict(2);                                                                % Present word
    wait(exp.times.word);                                                       % Wait
    drawpict(4);                                                                % Present blank screen
    wait(exp.times.blank1);                                                     % Wait
    
    clearkeys;                                                                  % Clear keys
    t = drawpict(3);                                                            % Present object
    wait(exp.times.obj);                                                        % Wait
    readkeys;                                                                   % Read keys
    [key, keytime, n] = getkeydown;                                             % Get key presses read by 'readkeys'
    
    drawpict(4);                                                                % Present blank screen
    wait(exp.times.blank2);                                                     % Wait
    
    % Record trial information
    exp.exp.stime(trial)        = t;                                            % Record object presentation time
    exp.exp.cond(trial)         = exp.cond(trial);                              % Record condition
    exp.exp.loc(trial)          = exp.loc(exp.loccond(trial));                  % Record object location
    exp.exp.categ(trial)        = exp.stim.categ(exp.stim.ord(trial));          % Record category of stimulus
    
    if n>0                                                                      % If keypress has been made than n>0
        exp.exp.key(trial)      = key(1);                                       % Record keypress
        exp.exp.keytime(trial)  = keytime(1);                                   % Record keypress time
    else                                                                        % If keypress hasn't been made
        exp.exp.key(trial)      = nan;                                          % Record keypress as 'nan'
        exp.exp.keytime(trial)  = nan;                                          % Record keypress time as 'nan'
    end
    
    save(exp.fname,'exp');                                                      % Save results file
end                                                                             % End trial loop

%% End Experiment

clearpict(1);                               % Clear buffer 1
preparestring('End of Experiment', 1);      % Prepare string in buffer 1
drawpict (1);                               % Present buffer 1
waitkeydown(inf,71);                        % Wait inf for keypress (space)
clearpict(1);                              	% Clear buffer 1

save(exp.fname,'exp');                      % Save results file

stop_cogent;                                % Stop Cogent
